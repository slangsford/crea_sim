data{
     int <lower=1> N;
     int <lower=1> J;
     int <lower=1> K;
     int <lower=1,upper=N> NN[K];
     int <lower=1,upper=J> JJ[K];
     int <lower=0,upper=2> Y[K];
     real<lower=0> T[K];
}

parameters {
  real theta[N];
  real b[J];
  real <lower=0>a[J];
  real <lower=0>gamma[N];
  real <lower=0.0000000001>tau[J]; // An error occurs if tau == 0 in wiener;
}

transformed parameters{
  real <lower=0> alpha[K];
  real v[K];
  real taus[K];
  for (k in 1:K){
    alpha[k] = (gamma[NN[k]]/a[JJ[k]]);
    taus[k] = tau[JJ[k]];
    if(Y[k]==1){
      v[k] = (theta[NN[k]]-b[JJ[k]]);
    } else {
      v[k] = (-theta[NN[k]]+b[JJ[k]]);
    }
  }
}
model {
  a ~ cauchy(0,5);
  gamma ~ lognormal(0,1);
  b ~ normal(0,2.5);
  theta ~ normal(0,1);
  tau ~ cauchy(0,5);
  
  T ~ wiener(alpha,taus,0.5,v);
}

generated quantities{
  vector[K] log_lik;
  for(k in 1:K){
    log_lik[k] = (
      wiener_lpdf(T[k]|(gamma[NN[k]]/a[JJ[k]]),tau[JJ[k]],0.5,( theta[NN[k]]-b[JJ[k]]))*(2-Y[k])+
      wiener_lpdf(T[k]|(gamma[NN[k]]/a[JJ[k]]),tau[JJ[k]],0.5,(-theta[NN[k]]+b[JJ[k]]))*(Y[k]-1)
      );
  }
}

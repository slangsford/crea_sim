functions{
     
     real lba_pdf(real t, real b, real A, real v, real s){
          //PDF of the LBA model
          
          real b_A_tv_ts;
          real b_tv_ts;
          real term_1;
          real term_2;
          real term_3;
          real term_4;
          real pdf;
          
          b_A_tv_ts = (b - A - t*v)/(t*s);
          b_tv_ts = (b - t*v)/(t*s);
          term_1 = v*Phi(b_A_tv_ts);
          term_2 = s*exp(normal_lpdf(b_A_tv_ts|0,1)); 
          term_3 = v*Phi(b_tv_ts);
          term_4 = s*exp(normal_lpdf(b_tv_ts|0,1)); 
          pdf = (1/A)*(-term_1 + term_2 + term_3 - term_4);
          
          return pdf;
     }
     
     real lba_cdf(real t, real b, real A, real v, real s){
          //CDF of the LBA model
          
          real b_A_tv;
          real b_tv;
          real ts;
          real term_1;
          real term_2;
          real term_3;
          real term_4;
          real cdf;	
          
          b_A_tv = b - A - t*v;
          b_tv = b - t*v;
          ts = t*s;
          term_1 = b_A_tv/A * Phi(b_A_tv/ts);	
          term_2 = b_tv/A   * Phi(b_tv/ts);
          term_3 = ts/A     * exp(normal_lpdf(b_A_tv/ts|0,1)); 
          term_4 = ts/A     * exp(normal_lpdf(b_tv/ts|0,1)); 
          cdf = 1 + term_1 - term_2 + term_3 - term_4;
          
          return cdf;
          
     }
     
     real lba_lpmf(int response, real RT, real b, real A, row_vector v, real s,real tau){
          
          real t;
          real cdf;
          real pdf;
          real prob;
          real out;
          real prob_neg;

               t = RT - tau;
               if(t > 0){			
                    cdf = 1;
                    
                    for(j in 1:num_elements(v)){
                         if(response == j){
                              pdf = lba_pdf(t, b, A, v[j], s);
                         }else{	
                              cdf = (1-lba_cdf(t, b, A, v[j], s)) * cdf;
                         }
                    }
                    prob_neg = 1;
                    for(j in 1:num_elements(v)){
                         prob_neg = Phi(-v[j]/s) * prob_neg;    
                    }
                    prob = pdf*cdf;		
                    prob = prob/(1-prob_neg);	
                    if(prob < 1e-10){
                         prob = 1e-10;				
                    }
                    
               }else{
                    prob = 1e-10;			
               }		
          return log(prob);		
     }
     
    vector lba_rng(real k, real A, vector v, real s){
          
          int get_pos_drift;	
          int no_pos_drift;
          int get_first_pos;
          vector[num_elements(v)] drift;
          int max_iter;
          int iter;
          real start[num_elements(v)];
          real ttf[num_elements(v)];
          int resp[num_elements(v)];
          real rt;
          vector[2] pred;
          real b;
          
          //try to get a positive drift rate
          get_pos_drift = 1;
          no_pos_drift = 0;
          max_iter = 1000;
          iter = 0;
          while(get_pos_drift){
               for(j in 1:num_elements(v)){
                    drift[j] = normal_rng(v[j],s);
                    if(drift[j] > 0){
                         get_pos_drift = 0;
                    }
               }
               iter = iter + 1;
               if(iter > max_iter){
                    get_pos_drift = 0;
                    no_pos_drift = 1;
               }	
          }
          //if both drift rates are <= 0
          //return an infinite response time
          if(no_pos_drift){
               pred[1] = -1;
               pred[2] = -1;
          }else{
               b = A + k;
               for(i in 1:num_elements(v)){
                    //start time of each accumulator	
                    start[i] = uniform_rng(0,A);
                    //finish times
                    ttf[i] = (b-start[i])/drift[i];
               }
               //rt is the fastest accumulator finish time	
               //if one is negative get the positive drift
               resp = sort_indices_asc(ttf);
               ttf = sort_asc(ttf);
               get_first_pos = 1;
               iter = 1;
               while(get_first_pos){
                    if(ttf[iter] > 0){
                         pred[1] = ttf[iter];
                         pred[2] = resp[iter]; 
                         get_first_pos = 0;
                    }
                    iter = iter + 1;
               }
          }
          return pred;	
     }
}

data{
     int <lower=1> N;
     int <lower=1> J;
     int <lower=1> K;
     int <lower=1,upper=N> NN[K];
     int <lower=1,upper=J> JJ[K];
     int<lower=0,upper=2> Y[K];
     real<lower=0> T[K];
}

parameters {
     real<lower=0> a [J];
     real<lower=0> gamma [N];
     real theta [N];
     real b [J];
     real<lower=0> sigma[N];
     real<lower=0> phi[J];
     real<lower=0> tau [J];
}

transformed parameters {
     matrix <lower=0,upper=1>[K,2] v ;
     for (k in 1:K){
         v[k,1] = inv_logit(theta[NN[k]]-b[JJ[k]]);
         v[k,2] = inv_logit(-theta[NN[k]]+b[JJ[k]]);
     }
}

model {
     a ~ cauchy(0,5);
     gamma ~ lognormal(0,1);
     b ~ normal(0,2.5);
     theta ~ normal(0,1);
     phi ~ cauchy(0,5);
     sigma ~ lognormal(0,1);
     tau ~ cauchy(0,5);
     
     for (k in 1:K){
         target += (lba_lpmf(Y[k]|T[k],gamma[NN[k]]/a[JJ[k]],0.5*gamma[NN[k]]/a[JJ[k]],v[k,],sigma[NN[k]]/phi[JJ[k]],tau[JJ[k]]));
     }
}

generated quantities{
  vector[K] log_lik;
  for(k in 1:K){
    log_lik[k] = lba_lpmf(Y[k]|T[k],gamma[NN[k]]/a[JJ[k]],0.5*gamma[NN[k]]/a[JJ[k]],v[k,],sigma[NN[k]]/phi[JJ[k]],tau[JJ[k]]);
  }
}

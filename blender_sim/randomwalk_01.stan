functions{
	real solvechallenge_lpmf(int n_actions, real difficulty, real skill){
	real mysteps[n_actions];
	for(anaction in 1:(n_actions)){
	mysteps[anaction] = normal_lcdf(difficulty | skill*anaction, anaction);#log prob of not hitting target.. note fixed sd of 1 (accumulates on each action, sum of normals)
	}
	mysteps[n_actions] = log(1-exp(mysteps[n_actions]));#do hit the target on the last action.
return (log_sum_exp(mysteps));
	}
}

data {
     int n_items;
     int n_ppnts;
     
     int n_trials;
     int item_id[n_trials];
     int ppnt_id[n_trials];
     int obs[n_trials];
}
parameters{
	real item_difficulty[n_items];
	real ppnt_skill[n_items];
}
model{
	item_difficulty~normal(5,1);#matches reality: todo infer pop level param.
	ppnt_skill~normal(2,1);#matches reality: toto infer pop level param.

	for(i in 1:n_trials){
	            obs[i]~solvechallenge_lpmf(item_difficulty[item_id[i]], ppnt_skill[ppnt_id[i]]);
	}
}

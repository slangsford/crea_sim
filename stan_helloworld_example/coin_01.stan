data {
     int n_flips;
     int flips[n_flips];
}
parameters{
	real theta;
}
model{
	theta~beta(1,1); //uniform prior.
	for(i in 1:n_flips){
		flips[i] ~ bernoulli(theta);
	}
}

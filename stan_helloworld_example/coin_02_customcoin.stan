functions{
	real mycoin_lpmf(int result, real theta){
	     return (
	     log(
	     result*theta+(1-result)*(1-theta)//evals to theta if result==1, or 1-theta if result=0
	     )
	     );
	}
}

data {
     int n_flips;
     int flips[n_flips];
}
parameters{
	real theta;
}
model{
	theta~beta(1,1);
	for(i in 1:n_flips){
		flips[i] ~ mycoin(theta);
	}
}
